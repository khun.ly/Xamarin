﻿using RetroGames.PlatformDependantServices;
using RetroGames.UWP.Services;
using SQLite;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Xamarin.Forms;
using System.Text;
using System.Threading.Tasks;

[assembly: Dependency(typeof(GetSQLLiteConnectionUWP))]
namespace RetroGames.UWP.Services
{
    
    class GetSQLLiteConnectionUWP : IGetSQLLiteConnection
    {
        public SQLiteConnection GetConnection()
        {
            string path = Path.Combine(
                Environment.GetFolderPath(
                    Environment.SpecialFolder.LocalApplicationData),
                "retro_game.db3");
            return new SQLiteConnection(path);
        }
    }
}
