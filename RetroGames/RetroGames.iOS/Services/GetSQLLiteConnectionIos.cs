﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using Foundation;
using RetroGames.iOS.Services;
using RetroGames.PlatformDependantServices;
using SQLite;
using UIKit;
using Xamarin.Forms;

[assembly: Dependency(typeof(GetSQLLiteConnectionIos))]
namespace RetroGames.iOS.Services
{
    class GetSQLLiteConnectionIos : IGetSQLLiteConnection
    {
        public SQLiteConnection GetConnection()
        {
            string path = Environment.GetFolderPath(
                    Environment.SpecialFolder.Personal
            );
            path = Path.Combine(path, "retro_game.db3");
            return new SQLiteConnection(path);
        }
    }
}