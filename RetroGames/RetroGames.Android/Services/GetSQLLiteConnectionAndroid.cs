﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using RetroGames.Droid.Services;
using RetroGames.PlatformDependantServices;
using SQLite;
using Xamarin.Forms;

[assembly: Dependency(typeof(GetSQLLiteConnectionAndroid))]
namespace RetroGames.Droid.Services
{
    class GetSQLLiteConnectionAndroid : IGetSQLLiteConnection
    {
        public SQLiteConnection GetConnection()
        {
            string path = System.Environment.GetFolderPath(
                    System.Environment.SpecialFolder.ApplicationData
            );
            path = Path.Combine(path, "retro_game.db3");
            return new SQLiteConnection(path);
        }
    }
}