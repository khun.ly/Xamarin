﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite;

namespace RetroGames.PlatformDependantServices
{
    public interface IGetSQLLiteConnection
    {
        SQLiteConnection GetConnection();
    }
}
