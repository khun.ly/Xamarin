﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Text;
using RetroGames.DAL.Repository;

namespace RetroGames.MVVM
{
    public class ServiceLocator
    {
        private static ServiceLocator instance;


        private static object the_lock = new object();
        public static ServiceLocator Instance
        {
            get
            {
                lock (the_lock)
                {
                    return instance = instance ?? new ServiceLocator();
                }
            }
        }
        private ServiceLocator()
        {
            _instanciatedServices = new Dictionary<Type, object>();
            RegisterSercices();
        }

        private readonly Dictionary<Type, object> _instanciatedServices;

        private Dictionary<Type, Type> _registeredServices;

        public TInterface GetService<TInterface>()
        {

            //regarder si le service a déjà présent dans les services enregistrés
            Type serviceType;
            _registeredServices.TryGetValue(typeof(TInterface), out serviceType);
            if (serviceType == null)
            {
                throw new ApplicationException("le service demandé n'est pas enregistré");
            }

            //regarder si le service a déjà présent dans les services instanciés
            object service;
            _instanciatedServices.TryGetValue(serviceType, out service);
            if (service != null)
            {
                return (TInterface)service;
            }
            ConstructorInfo ctor = serviceType.GetConstructors().FirstOrDefault();
            service  = ctor.Invoke(new object[0]);
            _instanciatedServices[serviceType] = service;
            return (TInterface)service;
        }

        private void RegisterSercices()
        {
            _registeredServices = new Dictionary<Type, Type>
            {
                { typeof(IUserRepository), typeof(UserRepository) },
                { typeof(IConsoleRepository), typeof(ConsoleRepository) },
                { typeof(IGameRepository), typeof(GameRepository) },
            };
        }
    }
}
