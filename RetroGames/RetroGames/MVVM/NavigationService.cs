﻿using RetroGames.ViewModels;
using RetroGames.Views;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using Xamarin.Forms;

namespace RetroGames.MVVM
{
    public class NavigationService
    {
        public NavigationService()
        {
            //RegisterRoutes();
        }


        private void ModalAddConsole(ConsolesViewModel sender)
        {
            App.Current.MainPage.Navigation.PushModalAsync(new AddConsoleView());
        }

        public void RegisterRoutes()
        {
            MessagingCenter.Subscribe <ConsolesViewModel> (
                this, "modalAddConsole", ModalAddConsole
            );

            MessagingCenter.Subscribe<ConsolesViewModel>(
                this, "modalAddGame", ModalAddGame
            );

            MessagingCenter.Subscribe<ConsoleViewModel>(
                this, "exitModal", ExitModal
            );
        }

        private void ModalAddGame(ConsolesViewModel obj)
        {
            App.Current.MainPage.Navigation.PushModalAsync(
                new AddGameView()
            );
        }

        private void ExitModal(ConsoleViewModel sender)
        {
            App.Current.MainPage.Navigation.PopModalAsync();
        }
    }
}
