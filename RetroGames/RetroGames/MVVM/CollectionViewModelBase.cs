﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace RetroGames.MVVM
{
    public abstract class CollectionViewModelBase<T>: ViewModelBase
    {
        private ObservableCollection<T> items;
        public ObservableCollection<T> Items {
            get {
                return items ?? (items = LoadItems());
            }
        }

        protected abstract ObservableCollection<T> LoadItems();
    }
}
