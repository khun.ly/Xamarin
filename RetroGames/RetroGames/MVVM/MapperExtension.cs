﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System.Linq;

namespace RetroGames.MVVM
{
    static class MapperExtension
    {
        public static TTo Map<TFrom, TTo>(this object from, params object[] parameters)
        {
            IEnumerable<PropertyInfo> p1 = 
                typeof(TFrom).GetProperties();
            IEnumerable<PropertyInfo> p2 =
                typeof(TTo).GetProperties();
            ConstructorInfo ctor = typeof(TTo)
                .GetConstructors().FirstOrDefault();
            TTo result = (TTo)ctor.Invoke(parameters);
            foreach (PropertyInfo pi1 in p1.Where(p => p.CanRead))
            {
                foreach(PropertyInfo pi2 in p2.Where(p => p.CanWrite))
                {
                    if (pi1.Name == pi2.Name)
                    {
                        pi2.SetValue(result, pi1.GetValue(from));
                    }
                }
            }
            return result;
        }
    }
}
