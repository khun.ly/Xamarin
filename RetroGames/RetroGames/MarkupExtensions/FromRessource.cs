﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RetroGames.MarkupExtensions
{
    [ContentProperty(nameof(Source))]
    public class FromRessource : IMarkupExtension
    {
        public string Source { get; set; }
        public object ProvideValue(IServiceProvider serviceProvider)
        {
            if (Source == null)
            {
                return null;
            }
            return ImageSource.FromResource(Source);
        }
    }
}
