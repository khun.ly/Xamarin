using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using SQLite;
using System.IO;
using RetroGames.Views;
using RetroGames.MVVM;
using RetroGames.PlatformDependantServices;

[assembly: XamlCompilation (XamlCompilationOptions.Compile)]
namespace RetroGames
{
	public partial class App : Application
	{
		public App ()
		{
			InitializeComponent();

            SQLiteConnection db 
                = DependencyService
                .Get<IGetSQLLiteConnection>().GetConnection();
            
            db.CreateTable<DAL.Models.Console>();
            db.CreateTable<DAL.Models.User>();
            db.CreateTable<DAL.Models.Eval>();
            db.CreateTable<DAL.Models.Game>();
            Properties.Add("connection", db);

            MainPage = new ConsolesView();
            NavigationService nav = new NavigationService();

            nav.RegisterRoutes();
		}

		protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}
