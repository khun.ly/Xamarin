﻿using RetroGames.DAL.Repository;
using RetroGames.MVVM;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Linq;
using M = RetroGames.DAL.Models;
using System.Windows.Input;
using Xamarin.Forms;

namespace RetroGames.ViewModels
{
    public class GameViewModel: ViewModelBase
    {
        public int GameID { get; set; }
        private string name;
        private DateTime releaseDate;
        private double price;
        private string editor;
        private int consoleID;

        private ConsoleViewModel selectedConsole;

        public ObservableCollection<ConsoleViewModel> Consoles { get; set; }

        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
                RaisePropertyChanged();
            }
        }

        public DateTime ReleaseDate
        {
            get
            {
                return releaseDate;
            }

            set
            {
                releaseDate = value;
                RaisePropertyChanged();
            }
        }

        public double Price
        {
            get
            {
                return price;
            }

            set
            {
                price = value;
                RaisePropertyChanged();
            }
        }

        public string Editor
        {
            get
            {
                return editor;
            }

            set
            {
                editor = value;
                RaisePropertyChanged();
            }
        }

        public int ConsoleID
        {
            get
            {
                return consoleID;
            }

            set
            {
                consoleID = value;
                RaisePropertyChanged();
            }
        }

        public ICommand AddGameCommand { get; }

        public ConsoleViewModel SelectedConsole
        {
            get
            {
                return selectedConsole;
            }

            set
            {
                selectedConsole = value;
                RaisePropertyChanged();
            }
        }

        public GameViewModel()
        {
            Consoles = new ObservableCollection<ConsoleViewModel>(
                ServiceLocator.Instance.GetService<IConsoleRepository>()
                .GetAll().Select(c => c.Map<M.Console, ConsoleViewModel>())
            );
            AddGameCommand = new Command(AddGame);
        }

        private void AddGame()
        {
            consoleID = SelectedConsole.ConsoleID;
            ServiceLocator.Instance.GetService<IGameRepository>().Insert(
                this.Map<GameViewModel, M.Game>()
            );
        }
    }
}
