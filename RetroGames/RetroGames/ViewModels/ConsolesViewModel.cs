﻿using RetroGames.DAL.Repository;
using RetroGames.MVVM;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Linq;
using RetroGames.DAL.Models;
using System.Windows.Input;
using Xamarin.Forms;
using RetroGames.Views;
//using System;

namespace RetroGames.ViewModels
{
    class ConsolesViewModel : CollectionViewModelBase<ConsoleViewModel>
    {
        protected override ObservableCollection<ConsoleViewModel> LoadItems()
        {
            return new ObservableCollection<ConsoleViewModel>(
                ServiceLocator.Instance.GetService<IConsoleRepository>()
                .GetAll()
                .Select(c => c.Map<Console, ConsoleViewModel>())
            );
        }

        public ICommand AddConsoleCommand { get; }
        public ICommand AddGameCommand { get; }

        private readonly INavigation _navigation;
        public ConsolesViewModel()
        {
            //_navigation = navigation;
            AddConsoleCommand = new Command(AddConsole);
            AddGameCommand = new Command(AddGame);
            MessagingCenter.Subscribe<ConsoleViewModel, ConsoleViewModel>(
                this, "addConsole", (sender, cvm) =>
                {
                    Items.Add(cvm);
                }
            );
            MessagingCenter.Subscribe<ConsoleViewModel, ConsoleViewModel>(
                this, "deleteConsole", (sender, cvm) =>
                {
                    Items.Remove(cvm);
                }
            );
        }

        private void AddGame()
        {
            MessagingCenter.Send<ConsolesViewModel>(this, "modalAddGame");
        }

        private void AddConsole()
        {
            MessagingCenter
                .Send<ConsolesViewModel>(this, "modalAddConsole");
        }
    }
}
