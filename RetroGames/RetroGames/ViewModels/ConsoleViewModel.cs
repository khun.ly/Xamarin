﻿using M = RetroGames.DAL.Models;
using RetroGames.DAL.Repository;
using RetroGames.MVVM;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;
using RetroGames.Views;

namespace RetroGames.ViewModels
{
    public class ConsoleViewModel : ViewModelBase
    {
        private int consoleID;

        public int ConsoleID
        {
            get { return consoleID; }
            set { consoleID = value; RaisePropertyChanged(); }
        }

        private string name;

        public string Name
        {
            get { return name; }
            set { name = value; RaisePropertyChanged(); }
        }

        public FileImageSource CloseImage { get; set; }

        public ICommand ExitCommand { get; }
        public ICommand SaveCommand { get; }
        public ICommand DeleteConsoleCommand { get; }
        public ICommand UpdateConsoleCommand { get; }
        public ICommand ConfirmUpdateCommand { get; }

        public ConsoleViewModel()
        {
            ExitCommand = new Command(Exit);
            SaveCommand = new Command(Save);
            DeleteConsoleCommand = new Command(Delete);
            UpdateConsoleCommand = new Command(Update);
            ConfirmUpdateCommand = new Command(ConfirmUpdate);
            //ImageSource src = FileImageSource.FromUri(
            //    new System.Uri(
            //        "https://www.freeiconspng.com/uploads/close-button-png-28.png"
            //        )
            //);
            //Image im = new Image { Source = src };
        }

        private void ConfirmUpdate()
        {
            ServiceLocator.Instance.GetService<IConsoleRepository>()
                .Update(this.Map<ConsoleViewModel,M.Console>());
            App.Current.MainPage.Navigation.PopModalAsync();
        }

        private void Update()
        {
            App.Current.MainPage.Navigation.PushModalAsync(
                new UpdateConsoleView(this)
            );
        }

        private async void Delete()
        {
            bool allow = await App.Current.MainPage.DisplayAlert(
                "Suppression",
                $"Êtes-vous sûr de vouloir supprimer la console {this.Name}",
                "oui",
                "non"
            );
            if (allow)
            {
                ServiceLocator.Instance.GetService<IConsoleRepository>().Delete(this.ConsoleID);
                MessagingCenter.Send<ConsoleViewModel, ConsoleViewModel>(
                    this, "deleteConsole", this); 
            }
        }

        private void Exit()
        {
            App.Current.MainPage.Navigation.PopModalAsync();
        }

        private void Save()
        {
            M.Console c = this.Map<ConsoleViewModel, M.Console>();
            ServiceLocator.Instance.GetService<IConsoleRepository>()
                .Insert(c);
            MessagingCenter
                .Send<ConsoleViewModel, ConsoleViewModel>(
                this, "addConsole", c.Map<M.Console, ConsoleViewModel>());
            App.Current.MainPage.Navigation.PopModalAsync();
        }

        public override string ToString()
        {
            return this.Name;
        }

    }
}
