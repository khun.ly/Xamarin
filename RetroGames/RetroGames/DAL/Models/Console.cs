﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace RetroGames.DAL.Models
{
    public class Console
    {
        [PrimaryKey, AutoIncrement]
        public int ConsoleID { get; set; }
        public string Name { get; set; }
    }
}
