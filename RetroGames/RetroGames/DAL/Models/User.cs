﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace RetroGames.DAL.Models
{
    public class User
    {
        [PrimaryKey, AutoIncrement]
        public int UserID { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
