﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace RetroGames.DAL.Models
{
    public class Game
    {
        [PrimaryKey, AutoIncrement]
        public int GameID { get; set; }
        public string Name { get; set; }
        public DateTime ReleaseDate { get; set; }
        public double Price { get; set; }
        public string Editor { get; set; }
        public int ConsoleID { get; set; }
    }
}
