﻿using RetroGames.DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;
using SQLite;
using System.Linq;

namespace RetroGames.DAL.Repository
{
    public class UserRepository : IUserRepository
    {
        private readonly SQLiteConnection db;
        public UserRepository()
        {
            db = (SQLiteConnection)App.Current.Properties["connection"];
        }

        public int Insert(User u)
        {
            return db.Insert(u);
        }

        public User Get(int ID)
        {
            return db.Get<User>(ID);
        }

        public IEnumerable<User> GetAll()
        {
            return db.Query<User>("SELECT * FROM [User]");
        }

        public void Delete(int ID)
        {
            db.Delete<User>(ID);
        }

        public void Update(User u)
        {
            db.Update(u);
        }
    }
}
