﻿using System.Collections.Generic;
using RetroGames.DAL.Models;

namespace RetroGames.DAL.Repository
{
    public interface IUserRepository
    {
        void Delete(int ID);
        User Get(int ID);
        IEnumerable<User> GetAll();
        int Insert(User u);
        void Update(User u);
    }
}