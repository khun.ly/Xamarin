﻿using RetroGames.DAL.Models;
using SQLite;
using System.Collections.Generic;
using System.Text;

namespace RetroGames.DAL.Repository
{
    class ConsoleRepository : IConsoleRepository
    {
        private readonly SQLiteConnection db;
        public ConsoleRepository()
        {
            db = (SQLiteConnection)App.Current.Properties["connection"];
        }

        public int Insert(Console c)
        {
            return db.Insert(c);
        }

        public Console Get(int ID)
        {
            return db.Get<Console>(ID);
        }

        public IEnumerable<Console> GetAll()
        {
            return db.Query<Console>("SELECT * FROM [Console]");
        }

        public void Delete(int ID)
        {
            db.Delete<Console>(ID);
        }

        public void Update(Console c)
        {
            db.Update(c);
        }
    }
}
