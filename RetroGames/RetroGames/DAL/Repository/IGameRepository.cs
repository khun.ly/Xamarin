﻿using System.Collections.Generic;
using RetroGames.DAL.Models;

namespace RetroGames.DAL.Repository
{
    public interface IGameRepository
    {
        void Delete(int ID);
        Game Get(int ID);
        IEnumerable<Game> GetAll();
        int Insert(Game g);
        void Update(Game g);
    }
}